module hello-run

go 1.13

require (
	cloud.google.com/go/firestore v1.6.0
	github.com/hashicorp/go-retryablehttp v0.7.0
	google.golang.org/api v0.57.0
)
